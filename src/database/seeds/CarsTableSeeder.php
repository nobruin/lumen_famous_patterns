<?php

use App\Http\Models\Car;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Car::create([
            'name' => Str::random(10),
            'description' => Str::random(150),
            'model' => Str::random(10)            
            ]
        );
    }
}
