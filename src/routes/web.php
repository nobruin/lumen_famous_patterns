<?php

use Laravel\Lumen\Routing\Router;

/** 
 * @var Router $router
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api'], function () use ($router){
    $router->group(['prefix' => 'cars'], function () use ($router){
        $router->post('', 'CarsController@store');
        $router->get('', 'CarsController@list');
        $router->get('{id}', 'CarsController@show');
        $router->put('{id}', 'CarsController@update');
        $router->delete('{id}', 'CarsController@destroy');
    });
});


