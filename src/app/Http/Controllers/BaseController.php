<?php

namespace App\Http\Controllers;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{    
    protected Model $model;

    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function store(Request $request)
    {                
        return \response()
                ->json(
                    $this->model->create($request->all()),
                    Response::HTTP_CREATED
                );
    }

    public function list(Request $request)
    {
        $entities = $this->model->paginate();

        return response()->json($entities, Response::HTTP_PARTIAL_CONTENT);
    }
    
    public function show(int $id)
    {
        $entite = $this->model->find($id);

        if(empty($entite)){
            return \response()->json(null, Response::HTTP_NOT_FOUND);
         }
 
         return \response()->json($entite, Response::HTTP_OK);
        
    }
    
    public function update(int $id, Request $request)
    {        
        $entite = $this->model->find($id)->update($request->all());

        if(empty($entite)){
            return \response()->json(null, Response::HTTP_NOT_FOUND);
         }
 
         return \response()->json($entite);
        
    }
    
    public function destroy(int $id)
    {
        $this->model->destroy($id);
        
         return \response()->json(null, Response::HTTP_OK);
        
    }
}
