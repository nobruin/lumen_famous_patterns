<?php

namespace App\Http\Controllers;

use App\Http\Models\Car;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Http\Request;

class CarsController extends BaseController
{  
    public function __construct(Car $car) {
        $this->model = $car;
    }   
}
